#include "message.hh"

message::message(std::string &str)
{

    std::stringstream ss(str);
    std::stringstream tmp;
    std::string word;
    ss >> word;
    if ((word != "renamex") || (word != "sendx"))
    {
        m_id = error;
    }
    id id_tmp = id_constructor(word);
    while (ss >> word)
    {
        tmp << word << " ";
    }
    std::string strx = tmp.str();
    m_id = id_tmp;
    m_data = strx;
}
std::string message::encode_message()
{
    std::string str;
    std::stringstream sstm;
    sstm << this->getID() << " " << this->getData();
    str = sstm.str();
    return str;
};

// CONSTRUCTOR PARAMETRYCZNY ZE STRINGA
message message::decode_message(std::string str)
{
    std::string strx;
    std::stringstream ss(str);
    std::stringstream tmp;
    std::string word;
    ss >> word;
    id id_tmp = id_constructor(word);
    while (ss >> word)
    {
        tmp << word << " ";
    }
    strx = tmp.str();
    message msg(id_tmp, strx);
    return msg;
};

std::ostream &operator<<(std::ostream &strm, const message &msg)
{
    strm << msg.m_id << " " << msg.m_data;
    return strm;
};