#ifndef MESSAGE_HH
#define MESSAGE_HH

#include <cstring>
#include <cstdint>
#include <vector>
#include <iostream>
#include <cstring>
#include <sstream>
#include <deque>

enum id
{
    error = 0,
    renamex = 1,
    sendx = 2
};
/*
id id_constructor(int idx)
{
    if (idx == 1)
    {
        return renamex;
    }
    if (idx == 2)
    {
        return sendx;
    }
    else
    {
        return error;
    }
};
*/

inline id id_constructor(std::string str)
{
    if (str == "renamex" || str == "1")
    {
        return renamex;
    }
    if (str == "sendx" || str == "2")
    {
        return sendx;
    }
    else
    {
        return error;
    }
};

class message
{
private:
    id m_id;
    std::string m_data;

public:
    message(){};
    message(std::string &str);
    message decode_message(std::string str);

    message(id id_x, std::string str)
    {
        m_id = id_x;
        m_data = str;
    };

    void setID(id idx) { m_id = idx; };
    int getID() { return m_id; };
    std::string getData() { return m_data; };
    std::string encode_message();
    void setData(std::string str)
    {
        m_data = str;
    }
    bool operator!=(message &msg);
    friend std::ostream &operator<<(std::ostream &strm, const message &msg);
    char *getchardata()
    {
        char *char_array = new char[m_data.length() + 1];
        strcpy(char_array, m_data.c_str());
        return char_array;
    };
    bool decode_header()
    {
        return true;
    }

    void encode_header()
    {
    }
};
typedef std::deque<message> message_queue;

#endif