#include <cstring>
#include <cstdint>
#include <vector>
#include <iostream>
#include <cstring>
#include <sstream>
#include <string>
#include <exception>
#include "server.hh"
#include <boost/asio.hpp>
int main()
{
    try
    {
        boost::asio::io_context io;
        tcp::endpoint endpoint(tcp::v4(), 8080);
        server server(io, endpoint);
        io.run();
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
    }
}