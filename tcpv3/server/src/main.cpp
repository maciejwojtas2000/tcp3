#include <iostream>
#include "server.hh"
#include <exception>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

int main()
{

    try
    {
        boost::asio::io_context io;
        
        tcp::endpoint endpoint(tcp::v4(), 8080);
        server server(io, endpoint);
        io.run();
    }    
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
    }
}