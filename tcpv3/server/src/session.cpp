#include "session.hh"

void session::start()
{
  m_room.join(shared_from_this());
  do_read_header();
}

void session::deliver(const message &msg)
{
  bool writing = !m_msgs_write.empty();

  m_msgs_write.push_back(msg);
  if (!writing)
  {
    do_write();
  }
};

void session::do_read_header()
{

  auto self(shared_from_this());

  boost::asio::async_read(m_socket,
                          boost::asio::buffer(m_msg_read.data(), message::header_lenght),
                          [this, self](boost::system::error_code ec, std::size_t /*length*/)
                          {
                            if (!ec && m_msg_read.decode_header())
                            {
                              do_read_body();
                            }
                            else
                            {
                              m_room.leave(shared_from_this());
                            }
                          });
}

void session::do_read_body()
{

  auto self(shared_from_this());

  boost::asio::async_read(
      m_socket,
      boost::asio::buffer(m_msg_read.body(), m_msg_read.body_lenght()),
      [this, self](boost::system::error_code ec, std::size_t)
      {
        if (!ec)
        {
          m_room.deliver(m_msg_read);
          do_read_header();
        }
        else
        {
          m_room.leave(shared_from_this());
        }
      });
}
void session::do_write()
{
  auto self(shared_from_this());
  boost::asio::async_write(
      m_socket,
      boost::asio::buffer(m_msgs_write.front().data(),
                          m_msgs_write.front().length()),
      [this, self](boost::system::error_code ec, std::size_t /*length*/)
      {
        if (!ec)
        {
          m_msgs_write.pop_front();
          if (!m_msgs_write.empty())
          {
            do_write();
          }
        }
        else
        {
          m_room.leave(shared_from_this());
        }
      });
}