#include "room.hh"

void chat_room::join(chat_participant_ptr participant)
{
    m_participants.insert(participant);

    for (auto msg : m_recent_msgs)
    {
        participant->deliver(msg);
    }
}

void chat_room::leave(chat_participant_ptr participant)
{
    m_participants.erase(participant);
}

void chat_room::deliver(const message &msg)
{
    m_recent_msgs.push_back(msg);

    while (m_recent_msgs.size() > max_recent_msgs)
    {
        m_recent_msgs.pop_front();
    }
    for (auto participant : m_participants)
    {
        participant->deliver(msg);
    }
}
