#ifndef ROOM_HH
#define ROOM_HH

#include "participant.hh"
#include <set>

class chat_room
{
private:
    std::set<chat_participant_ptr> m_participants;
    enum
    {
        max_recent_msgs = 100
    };
    message_queue m_recent_msgs;

public:
    void join(chat_participant_ptr participant);
    void leave(chat_participant_ptr participant);
    void deliver(const message &msg);
};

#endif