#ifndef SERVER_HH
#define SERVER_HH

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "session.hh"
#include<iostream>

using boost::asio::ip::tcp;

class server
{
private:
    tcp::acceptor m_acceptor;
    tcp::socket m_socket;
    chat_room m_room;

private:
    void do_accept(){
         m_acceptor.async_accept(
        m_socket,
        [this](boost::system::error_code ec)
        {
            if (!ec)
            {
                std::make_shared<session>(std::move(m_socket), m_room)->start();
            }
            do_accept();
        });
    }

public:
    server(boost::asio::io_context &io, const tcp::endpoint &endpoint) 
    : m_acceptor(io, endpoint)
    , m_socket(io)
    {
        std::cout<<"Start running server"<<std::endl;
        do_accept();
    }
    ~server(){};
};

#endif