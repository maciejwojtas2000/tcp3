#ifndef CHAT_SESSION_HH
#define CHAT_SESSION_HH

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "room.hh"

#include <boost/asio.hpp>
using boost::asio::ip::tcp;

class session : public chat_participant,
                public std::enable_shared_from_this<session>
{
private:
    tcp::socket m_socket;
    chat_room &m_room;
    message m_msg_read;
    message_queue m_msgs_write;

public:
    session(tcp::socket socket, chat_room &room)
        : m_socket(std::move(socket)),
          m_room(room) {}
    void start();

    void deliver(const message &msg);
    void do_read_header();
    void do_write();
    void do_read_body();
};

#endif