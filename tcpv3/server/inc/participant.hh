#ifndef PARTICIPANT_HH
#define PARTICIPANT_HH

#include "message.hh"
#include <cstdlib>
#include <memory>

class chat_participant
{
public:
    virtual void deliver(const message &msg) = 0;
    virtual ~chat_participant(){};
};

typedef std::shared_ptr<chat_participant> chat_participant_ptr;

#endif