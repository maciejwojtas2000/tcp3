
/*
enum id
{
    error = 0,
    renamex = 1,
    sendx = 2
};

id constructor(int idx)
{
    if (idx == 1)
    {
        return renamex;
    }
    if (idx == 2)
    {
        return sendx;
    }
    else
    {
        return error;
    }
};

id constructor(std::string str)
{
    if (str == "renamex")
    {
        return renamex;
    }
    if (str == "sendx")
    {
        return sendx;
    }
    else
    {
        return error;
    }
};

class message
{
private:
    id m_id;
    std::string m_data;

public:
    message(){};
    message(std::string str)
    {

        std::stringstream ss(str);
        std::stringstream tmp;
        std::string word;
        ss >> word;
        if ((word != "renamex") || (word != "sendx"))
        {
            m_id = error;
        }
        id idx3 = constructor(word);
        while (ss >> word)
        {
            tmp << word << " ";
        }
        std::string strx = tmp.str();
        m_id = idx3;
        m_data = strx;
    }

    message(id id_x, std::string str)
    {
        m_id = id_x;
        m_data = str;
    };

    void setID(id idx) { m_id = idx; };
    int getID() { return m_id; };
    std::string getData() { return m_data; };
    std::string encode_message1();
    message getMessage(std::istream &strm);
    void copyData(std::string str)
    {
        m_data = str;
    }

    friend std::ostream &operator<<(std::ostream &strm, const message &msg)
    {
        strm << msg.m_id << " " << msg.m_data;
        return strm;
    }
};

std::string message::encode_message1()
{
    std::string str;
    std::stringstream sstm;
    sstm << this->getID() << " " << this->getData();
    str = sstm.str();
    return str;
};

// CONSTRUCTOR PARAMETRYCZNY ZE STRINGA
message decode_message_x(std::string str)
{
    int idx;
    std::string strx;
    std::stringstream ss(str);
    std::stringstream tmp;

    std::string word;
    ss >> word;
    idx = std::stoi(word);
    id idx2 = constructor(idx);
    while (ss >> word)
    {
        tmp << word << " ";
    }
    strx = tmp.str();
    message msg(idx2, strx);
    return msg;
};

int main()
{
    std::string stre;
    while (std::getline(std::cin, stre))
    {
        message msg(stre);
        std::string str = msg.encode_message1();
        std::cout << str << std::endl;

        message msg1 = decode_message_x(str);
        std::string sttt = msg1.encode_message1();
        std::cout << sttt << std::endl;
    }
}*/
#include <iostream>
#include "server.hh"
#include <exception>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

int main()
{

    try
    {
        boost::asio::io_context io;
        tcp::endpoint endpoint(tcp::v4(), 8080);
        server server(io, endpoint);
        io.run();
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
    }
}