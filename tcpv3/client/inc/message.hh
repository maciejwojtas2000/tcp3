#ifndef MESSAGE_HH
#define MESSAGE_HH

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <deque>

class message
{
public:
    enum
    {
        header_lenght = 4
    };
    enum
    {
        max_body_lenght = 512
    };

private:
    char m_data[header_lenght + max_body_lenght];
    std::size_t m_body_lenght;

public:
    message() : m_body_lenght(0) {}

    const char *data() const { return m_data; }

    char *data() { return m_data; }
    std::size_t length() const
    {
        return header_lenght + m_body_lenght;
    }

    const char *body() const
    {
        return m_data + header_lenght;
    }

    char *body()
    {
        return m_data + header_lenght;
    }

    std::size_t body_lenght() const
    {
        return m_body_lenght;
    }

    void body_lenght(std::size_t new_lenght)
    {
        m_body_lenght = new_lenght;
        if (m_body_lenght > max_body_lenght)
        {
            m_body_lenght = max_body_lenght;
        }
    }

    bool decode_header()
    {
        char header[header_lenght + 1] = "";
        std::strncat(header, m_data, header_lenght);
        m_body_lenght = std::atoi(header);
        if (m_body_lenght > max_body_lenght)
        {
            m_body_lenght = 0;
            return false;
        }
        return true;
    }

    void encode_header()
    {
        char header[header_lenght + 1] = "";
        std::sprintf(header, "%4d", static_cast<int>(m_body_lenght));
        std::memcpy(m_data, header, header_lenght);
    }
};
typedef std::deque<message> message_queue;
#endif