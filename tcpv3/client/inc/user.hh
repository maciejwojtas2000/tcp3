#ifndef USER_HH
#define USER_HH

#include <fstream>
#include "hash.hh"
#include <memory>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>
#include "mysql/mysql.h"

std::string load_login()
{
    std::cout << "Podaj login:" << std::endl;
    std::string name;
    std::cin >> name;
    return name;
}


std::vector<std::string> init_banned(MYSQL *conn)
{
    std::vector<std::string> v1;
    MYSQL_ROW row;
    MYSQL_RES* res;
    if(conn)
        {

            int qstate=mysql_query(conn, "Select* FROM users");
            if(!qstate)
                {
                    res=mysql_store_result(conn);
                    int count=mysql_num_fields(res);
                    while(row=mysql_fetch_row(res))
                        {
                            std::string strx="1";
                            std::stringstream x;
                            x<<row[2];
                            std::string y;
                            x>>y;

                                
                                if(y==strx)
                                {
                                    std::stringstream banned_stream;
                                    banned_stream<<row[0];
                                    std::string ban_user;
                                    banned_stream>>ban_user;
                                    v1.push_back(ban_user);
                                }
                        }
                }
        }
        return v1;
}

std::vector<std::string> init_muted(MYSQL *conn)
{
    std::vector<std::string> v1;
    MYSQL_ROW row;
    MYSQL_RES* res;
    if(conn)
        {

            int qstate=mysql_query(conn, "Select* FROM users");
            if(!qstate)
                {
                    res=mysql_store_result(conn);
                    int count=mysql_num_fields(res);
                    while(row=mysql_fetch_row(res))
                        {
                            std::string strx="1";
                            std::stringstream x;
                            x<<row[3];
                            std::string y;
                            x>>y;

                                
                                if(y==strx)
                                {
                                    std::stringstream muted_stream;
                                    muted_stream<<row[0];
                                    std::string muted_user;
                                    muted_stream>>muted_user;
                                    v1.push_back(muted_user);
                                }
                        }
                }
        }
        return v1;
}


std::vector<std::string> init_passwords(MYSQL *conn)
{
    std::vector<std::string> v1;
    MYSQL_ROW row;
    MYSQL_RES* res;
    if(conn)
        {

            int qstate=mysql_query(conn, "Select* FROM users");
            if(!qstate)
                {
                    res=mysql_store_result(conn);
                    int count=mysql_num_fields(res);
                    while(row=mysql_fetch_row(res))
                        {

                            std::stringstream x;
                            x<<row[1];
                            std::string y;
                            x>>y;
                            v1.push_back(y);
                        }
                }
        }
        return v1;
}

std::vector<std::string> init_names(MYSQL *conn)
{
    std::vector<std::string> v1;
    MYSQL_ROW row;
    MYSQL_RES* res;
    if(conn)
        {

            int qstate=mysql_query(conn, "Select* FROM users");
            if(!qstate)
                {
                    res=mysql_store_result(conn);
                    int count=mysql_num_fields(res);
                    while(row=mysql_fetch_row(res))
                        {

                            std::stringstream x;
                            x<<row[0];
                            std::string y;
                            x>>y;
                            v1.push_back(y);
                        }
                }
        }
        return v1;
}



bool log_init(std::string name,MYSQL *conn)
{
    std::vector<std::string>names=init_names(conn);
    std::vector<std::string> passwords=init_passwords(conn);
    std::vector<std::string>banned=init_banned(conn);
    int iter = 0;

    for (int i = 0; i < names.size(); i++)
    {
        if (strcmp(name.c_str(), names[i].c_str()) == 0)
        {
            iter = i;
         
        }
    }

    std::string password;
    std::cout << "Podaj haslo" << std::endl;
    std::cin >> password;
    const char *hash_password1 = str2md5(password);

    for(int k=0;k<banned.size();k++)
    {
        if(strcmp(name.c_str(),banned[k].c_str())==0)
        {
            std::cout<<"Jesteś zbanowany"<<std::endl;
            return 1;
        }   
    }

    if (!strcmp(hash_password1, passwords[iter].c_str()) == 0)
    {
        std::cout<<"Nie zalogowano"<<std::endl;
        return 1;
    }
    std::cout << "Zalogowano" << std::endl;
    return 0;
}
#endif