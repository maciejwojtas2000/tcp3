#ifndef CLIENT_HH
#define CLIENT_HH

#include <deque>
#include <iostream>
#include <thread>
#include <boost/asio.hpp>
#include <cstdlib>
#include "message.hh"
#include <fstream>
using boost::asio::ip::tcp;

class client
{
private:
    boost::asio::io_service &io;
    tcp::socket m_socket;
    message m_read_msg;
    message_queue m_write_msg;
    public:
    std::vector<std::string> banned_users;

private:
    // Async. establishing connection
public:
    // Constructor uses do_connect function to establish connection during creation of the object
    client(boost::asio::io_service &iox, tcp::resolver::iterator endpoint_iterator, std::vector<std::string>v1) : io(iox),
                                                                                      m_socket(iox)
    {
        banned_users=v1;

        do_connect(endpoint_iterator);
    }
    ~client(){};

    void close();
    void write(const message &msg);
    void do_connect(tcp::resolver::iterator endpoint_iterator);
    void do_read_header();
    void do_read_body();
    void do_write();
};

#endif