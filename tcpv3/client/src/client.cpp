#include "client.hh"
void client::write(const message &msg)
{
    io.post(
        [this, msg]()
        {
            bool writing = !m_write_msg.empty();
            m_write_msg.push_back(msg);
            if (!writing)
            {
                do_write();
            }
        });
}

void client::close()
{
    io.post([this]()
            { m_socket.close(); });
}
void client::do_connect(tcp::resolver::iterator endpoint_iterator)
{
    boost::asio::async_connect(m_socket,
                               endpoint_iterator,
                               [this](boost::system::error_code ec, tcp::resolver::iterator)
                               {
                                   if (!ec)
                                   {
                                       do_read_header();
                                   }
                               });
}
void client::do_read_header()
{
    boost::asio::async_read(m_socket,
                            boost::asio::buffer(m_read_msg.data(), message::header_lenght),
                            [this](boost::system::error_code ec, std::size_t)
                            {
                                if (!ec && m_read_msg.decode_header())
                                {
                                    do_read_body();
                                }
                                else
                                {
                                    m_socket.close();
                                }
                            });
}

void client::do_read_body()
{

    boost::asio::async_read(m_socket,
                            boost::asio::buffer(m_read_msg.body(), m_read_msg.body_lenght()),
                            [this](boost::system::error_code ec, std::size_t)
                            {
                                if (!ec)
                                {
                                  
                                    int flag=1;
                                    std::string _tmp(m_read_msg.body());
                                    
                                    
                                    for(int i=0;i<banned_users.size();i++)
                                    {
                                        std::string nazwa_zbanowanego=banned_users[i];
                                    
                                        int wynik=_tmp.find(nazwa_zbanowanego);
                                      
                                        if(wynik!=std::string::npos)
                                        {
                                        flag=0;
                                        }
                                    } 



                                    if(flag==1)
                                    {
                                    std::cout.write(m_read_msg.body(), m_read_msg.body_lenght());
                                            std::cout << "\n";
                                            do_read_header();
                                    }
                                    else{
                                    do_read_header();
                                    }
                                }
                                else
                                {
                                    m_socket.close();
                                }
                            });
}
void client::do_write()
{
    boost::asio::async_write(m_socket,
                             boost::asio::buffer(m_write_msg.front().data(),
                                                 m_write_msg.front().length()),
                             [this](boost::system::error_code ec, std::size_t)
                             {
                                 if (!ec)
                                 {
                                     m_write_msg.pop_front();
                                     if (!m_write_msg.empty())
                                     {
                                         do_write();
                                     }
                                 }
                                 else
                                 {
                                     m_socket.close();
                                 }
                             });
}