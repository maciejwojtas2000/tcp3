#include "client.hh"
#include "message.hh"
#include <fstream>
#include "hash.hh"
#include "user.hh"
#include <vector>
#include <memory>
#include "mysql/mysql.h"

const char* hostname="127.0.0.1";
const char* username="root";
const char* password="";
const char* database="users";
unsigned port=3100;
const char* unixsocket=NULL;
unsigned long clientflag=0;


MYSQL* connect_to_database()
{
    MYSQL *conn;
    conn=mysql_init(0);
    conn=mysql_real_connect(conn,hostname,username,password,database,port,unixsocket,clientflag);

    if(conn){
        std::cout<<"Connected to database"<<std::endl;
        return conn;
    }
    else{
        std::cout<<"Can't connect"<<std::endl;
        return conn;
    }
}

int main()
{
    try
        {
        MYSQL *conn=connect_to_database();
        std::string name = load_login();

        if (log_init(name,conn))
        {
            return 1;
        }

    
        std::vector<std::string>muted=init_muted(conn);
        boost::asio::io_service io;
        tcp::resolver resolver(io);

        auto endpoint_iterator = resolver.resolve("127.0.0.1", "8080");
        client c(io, endpoint_iterator, muted);
        c.banned_users.push_back(name);
        std::thread t([&io]()
                      { io.run(); });

        char line[message::max_body_lenght + 1];

        while (std::cin.getline(line, message::max_body_lenght + 1))
        {
            message msg;
            char *line_dest = (char *)malloc(sizeof(char) * 100);
            std::strcpy(line_dest, name.c_str());
            std::strcat(line_dest, ": ");
            std::strcat(line_dest, line);
            msg.body_lenght(strlen(line_dest));
            std::memcpy(msg.body(), line_dest, msg.body_lenght());
            msg.encode_header();
            c.write(msg);
        }
        c.close();
        t.join();
    }

    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
    }
}
